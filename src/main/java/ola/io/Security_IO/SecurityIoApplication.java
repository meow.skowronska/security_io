package ola.io.Security_IO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityIoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityIoApplication.class, args);
	}

}
